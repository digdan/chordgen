<?
/**
*	music_helper - Library for computing music theroy
*
*
*/
	class music_helper extends array_wheel {
		var $key_note;
		var $wheel = array("C","C#","D","D#","E","F","F#","G","G#","A","A#","B");
		var $key_maps = array(
			0=>array("name"=>"I","mode"=>"major","ext"=>array("maj","maj7","maj9","maj11","maj13")),
			2=>array("name"=>"ii","mode"=>"minor","ext"=>array("min","min7","min9","min11","min6")),
			4=>array("name"=>"iii","mode"=>"minor","ext"=>array("min","min7")),
			5=>array("name"=>"IV","mode"=>"major","ext"=>array("maj","maj7","maj9","maj13","6")),
			7=>array("name"=>"V","mode"=>"major","ext"=>array("7","9","11","sus4","13")),
			9=>array("name"=>"vi","mode"=>"minor","ext"=>array("min","min7","min9","min11","min6")),
			11=>array("name"=>"vii","mode"=>"dim","ext"=>array("dim","min7b5")),
		);

		var $chord_maps = array(
			"maj"=>array(0,4,7),
			"min"=>array(0,3,7),
		);

		var $keys; //Rendered keys will be stored

/**
*	Constructor
*
*/
		function music_helper($key_note=NULL) {
			//Populate array wheel
			foreach($this->wheel as $note) $this->add($note);

			//Prebuild reverse key lookups
			foreach($this->wheel as $note) {
				$this->turn_to($note);
				foreach($this->key_maps as $position=>$key_data) {
					$this->keys[$note][] = $this->get($position);
				}
			}

			if ( ! is_null($key_note)) {
				return $this->set_keynote($key_note);
			}
		}

/**
* Sets the base note and key
*
* @param string $key_note note from the $this->wheel array
*/
		function set_keynote($key_note) {
			if ( ! in_array($key_note,$this->wheel)) { //Check against own values
				return false;
			}
			if ($this->turn_to($root)) {
				$this->key_note = $key_note;
				return true;
			} else {
				return false;
			}
		}

		function chord($chord_name,$note=NULL) {
			if (is_null($note)) {
				$note = $this->key_note;
			}
			if ( ! array_key_exists($chord_name,$this->chord_maps)) return false; //Only known chord names
			$this->turn_to($note); //Set to note
			$steps = $this->chord_maps[$chord_name];
			$notes = array();
			foreach($steps as $step) $notes[$step] = $this->get($step); //Define notes by their steps
			$this->turn_to($this->key_note); //Set wheel back
			return $notes;
		}


		function find_key($notes) {
			$ok = array();
			$out = array();
			foreach($this->keys as $key=>$key_info) $ok[$key] = true;
			foreach($notes as $note) {
				foreach($this->keys as $key_root=>$key) {
					if ( ! in_array($note,$key) ) {
						$ok[$key_root] = false;
					}
				}
			}

			if (is_array($ok)) {
				foreach($ok as $k=>$v) {
					if ($v) $out[] = $k;
				}
			}
			var_dump($out);
			return $out;
		}

	}
?>