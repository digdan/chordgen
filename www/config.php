<?php
	$config = array(
		'templates'=>'templates',
		'db'=>array(
			'dsn'=>'mysql:dbname=chordgen;host=127.0.0.1',
			'user'=>'chordgen',
			'password'=>'5wC7stWGbwdE7UaB',
		),
		'encryption'=>array(
			'salt'=>'se',
			'iterations'=>10
		),
		'sessions'=>array(
			'threshold'=>3600,
		),
		'users'=>array(
			'forgot_token_life'=>360, //How often a requested token can change
			'forgot_token_size'=>10, //Up to 32
		),
		'register_redirect'=>'/'
	);
?>
