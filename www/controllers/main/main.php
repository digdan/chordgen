<?
	class control_main {
		static function HGInit() {
			HG::map( "GET" , "/" , "control_main#front");
			HG::map( "GET" , "/services" , "control_main#services");
			HG::map( "GET" , "/portfolio" , "control_main#portfolio");
			HG::map( "GET" , "/contact" , "control_main#contact");
			HG::map( "GET" , "/chord" , "control_main#chord");
		}

		function front() {
			HG::chain()->
			active("home")->
			display("front.php",true);
		}

		function portfolio() {
			HG::chain()->
			v("meta.title","InterWebDev - Portfolio of websites")->
			active("portfolio")->
			display("portfolio.php",true);
		}

		function services() {
			HG::chain()->
			v("meta.title","InterWebDev - Service we Offer")->
			active("services")->
			display("services.php",true);
		}


		function contact() {
			HG::chain()->
			v("meta.title","InterWebDev - Contact Us")->
			v("message","")->
			active("contact")->
			display("contact.php",true);
		}


		function chord() {
			$rn = array(
				"I"=>0,
				"II"=>1,
				"III"=>2,
				"IV"=>3,
				"V"=>4,
				"VI"=>5,
				"VII"=>6,
				"VIII"=>7,
			);
			$out = array();
			$m = new music();
			$key = $_REQUEST["k"];
			$scale= $_REQUEST["scale"];
			$pos = $_REQUEST["pos"];
			$chord = $_REQUEST["chord"];
			$s = $m->get_scale_by_name($key,$scale);
			$base = $s[$rn[$pos]];
			$out["scale"] = $s;
			$out["notes"] = $m->get_chord_by_name($base,$chord);
			$out["chord"] = $base;
			switch($chord) {
				case "minor" : $out["chord"] .= "m"; break;
				case "major" : break;
				default : $out["chord"] .= $chord; break;
			}

			$note_map = $m->get_notes();

			//Build Piano
			for ($i=0;$i<=96;$i++) { //Begin Voodoo
				$note_pos = $i % 12;
				$note_test = $note_map[ $note_pos ];
				if (in_array( $note_test , $out["notes"] )) {
					if ($note_pos == 0 ) {
						$out["piano"][$i] = 1;
					} else {
						$out["piano"][$i] = 2;
					}
				} else {
					if ($note_pos == 0) {
						$out["piano"][$i] = 3;
					}
				}
			} // End Voodoo

			echo json_encode($out);
			die();
		}

	}
?>