<!DOCTYPE html>
<html lang="en">
	<head>
		<title><?= @$meta["title"];?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
		<link rel="stylesheet" href="/assets/css/bootstrap.css">
		<link rel="stylesheet" href="/assets/css/custom.css">
		<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css">
		<link rel="stylesheet" href="/assets/css/bootstrap-responsive.css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
		<script src="/assets/js/raphael.js" type="text/javascript"></script>
		<script src="/assets/js/jtab.js" type="text/javascript"></script>
		<script src="/assets/js/jquery.ui-custom.js" type="text/javascript"></script>
		<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="/assets/js/jquery.circleMenu.js" type="text/javascript"></script>
		<script src="/assets/js/piano.js" type="text/javascript"></script>
		<script src="/assets/js/jquery.form.js" type="text/javascript"></script>
	</head>

	<body>
		<div class="main container <?=$active["current"];?>">
			<header id="header" class="header">
				<div class="row">
					<div class="span12">
						<div class="pull-left logo">
							<A HREF="/"><i class="icon-list-ul"></i>ChordGen</A>
						</div>
						<div class="pull-right">
							<ul class="nav nav-pills" id="headerNav">
								<li class="<?=@$active["home"];?>"><a href="/"> Home </a></li>
								<li class="<?=@$active["contact"];?>" ><a href="/contact"> Contact </a></li>
							</ul>
						</div>
					</div>
				</div>
			</header>
