<H3>Contact Us</H3>
<P>

</P>
<? if ($message) {
	echo "<DIV class=\"row\"><DIV class=\"span12 alert\">{$message}</DIV></DIV>\n";
} ?>
<form class="well span11" action="?" method="POST">
  <div class="row">
		<div class="span6">
			<label>Email Address</label>
			<input type="text" name="email" class="span3" placeholder="Your email address">
		</div>
		<div class="span5">
			<label>Message</label>
			<textarea name="message" id="message" class="input-xlarge span5" rows="10"></textarea>
		</div>
		<button type="submit" class="btn btn-primary pull-right">Send</button>
	</div>
</form>