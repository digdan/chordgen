		</div>
		<div class="subfooter">
			<div class="container">
				<div class="span12">
					<article class="span4">
						<h3 class="color_white">Coming Soon...</h3>
						<p> <B>Chord Progression Library</B> - Use ChordGen to build and find chord progressions.</p>
						<p> <B>Melody Note Pathing</B> - Melody analytical statistics library.</p>
					</article>

					<article class="span4">
						<h3 class="color_white">Recent Articles</h3>
						<p> <i> No Articles Listed </i> </p>
					</article>

					<article class="span3">
						<h3 class="color_white">Reach Us Here</h3>
							<A HREF="mailto:info@chordgen.com">info@chordgen.com</A>
					</article>
				</div>
			</div>
		</div>

		<div class="footer">
			<div class="container">
				 &copy; Copyright  <?= date("Y"); ?> InterWeb Development
			</div>
		</div>

		<script type="text/javascript">

		</script>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-19546749-3', 'chordgen.com');
		  ga('send', 'pageview');

		</script>
	</body>
</html>