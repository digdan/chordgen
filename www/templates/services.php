<H2>Services</H2>
<div class="row">

	<div class="span6">
		<H3>Websites</H3>
		<P>We create attractive and engaging websites that will excite your customers. Our process involves understanding your vision and crafting an experience that will exceed your expectations. We use an iterative process to continually tweak and improve your website based on research, feedback and activity.</P>
		<ul>
		    <li>Planning & research</li>
		    <li>Information architecture</li>
		    <li>Wireframes & sketches</li>
		    <li>Visual design</li>
		    <li>HTML production</li>
		</ul>
	</div>

	<div class="span5">
		<H3>Design</H3>
		<P>You have less than 3 seconds to keep a user on your website. How will you manage to grab their attention? Our designers are experts in driving conversion using captivating imagery and interactive layouts.</P>
	</div>

	<div class="span6">
		<H3>Marketing</H3>
		<P>In addition to SEO, Vento's award-winning services include other forms of online marketing. We offer web public relations, reputation management, and pay-per-click (PPC) campaigns.</P>
	</div>

	<div class="span6">
	<H3>Consulting</H3>
	<P>Stuck on a current project? Need a second opinion? Need help turning an idea into a full-fledged website? We've launched startups for clients and ourselves having learned a thing or two along the way. We can provide value by fixing your pain points in order to help your business grow. Whether it's answering questions, making decisions or analyzing your website, We are here to help.</P>
	<ul>
	    <li>User experience</li>
	    <li>Design strategy</li>
	    <li>Startup assistance</li>
	    <li>E-commerce strategy</li>
	    <li>Conversion research & optimization</li>
	</ul>
	</div>

	<div class="span6">
		<H3>Programming / Coding</H3>
		<P>
			Our talented team know their technology. We offer a myriad of technological backend services.
		</P>
		<UL>
			<LI>PHP</LI>
			<LI>HTML5/CSS3</LI>
			<LI>Javqascript - jQuery,Ember,Backbone, etc.</LI>
			<LI>API, JSON/ XML & AJAX</LI>
			<LI>CMS - Wordpress/Druapl/Others</LI>
			<LI>Web Development</LI>
			<LI>UX/Interface Design</LI>
			<LI>Web Application Development</LI>
			<LI>Content Managment Systems</LI>
			<LI>Microsites</LI>
			<LI>Ecommerce</LI>
		</UL>
	</div>


	<div class="span6">
		<H3>Data</H3>
		<p></p>
		<UL>
			<LI>Data aggregation</LI>
			<LI>Data Scraping</LI>
			<LI>API Construction</LI>
		</UL>
	</div>

</div>