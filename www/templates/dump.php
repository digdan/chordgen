<P>Please enter your proxies in the box below. Currently we support HTTP and SOCKS proxies only. Please be sure to save your token to redeem your top proxies after 24 hours. <a href="/what-is-proxy-dumpster">Why would you dump proxies?</a></P>
<FORM METHOD="POST" ACTION="?">
<TEXTAREA name="proxies" cols="60" rows="10"></TEXTAREA>
        <P>Format: <I>IP:PORT</I><BR>Format: <I>USERNAME@IP:PORT</I><BR>Format: <I>USERNAME:PASSWORD@IP:PORT</I>
        </P>
        <INPUT TYPE="submit" name="cmd" value="Add Proxies">
</FORM>
<P>
By dumping proxies into the proxy dumpster you are allowing this service to scan, validate, and weed out the dead proxies. Once these proxies have been deemed as usable then they will be displayed on the free proxy list page. The best proxies are kept private, and only shared with those who dump proxies.
</P>