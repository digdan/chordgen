<?
	HG::v("meta",array(
		"title"=>"ChordGen - Music Theroy and Chord Generation Helper App",
	));
	$m = new music();
	$current_scale = "major";
	$current_chord = "major";
?>

<div class="row text-center">
	<div class="span6 spec">
		<form method="post" action="/key" >
			<H2>Key/Scale</H2>
			<div id="keySelectWrapper" class="span5">
				<ul id="keySelect">
					<li><a href="#" id="keySelectButton">C</a></li>
			<?
				foreach ($m->get_notes() as $note) {
					echo "\t\t<li data-note=\"{$note}\"><a href=\"#\">{$note}</a></li>";
				}
			?>
				</ul>
				<select name="scale" id="scale" onChange="buildChord();">
			<?foreach ($m->get_scale_types() as $scale) {
					echo "\t\t<option value=\"{$scale}\" ";
					if ($current_scale == $scale) echo "SELECTED";
					echo ">{$scale}</option>";
				}?>
				</select>
			</div>
		</form>
	</div>

<?
	$scaleNotes = $m->get_scale_by_name("C",$current_scale);
?>
	<div class="span6 spec">
		<form method="post" action="/chord" >
			<H2>Chord</H2>
			<div id="chordSelectWrapper" class="span5 offset1">
				<ul id="chordSelect">
					<li><a href="#" id="chordSelectButton">I</a></li>
				<? for($i=1;$i<=8;$i++) {
					echo "<li data-note=\"{$scaleNotes[$i-1]}\" data-index=\"{$i}\" data-chord=\"{$m->get_roman($i)}\"><a href=\"#\">{$m->get_roman($i)}</a></li>";
				} ?>
				</ul>

				<select name="chord" id="chord" onChange="buildChord();">
			<?foreach ($m->get_chord_types() as $chord) {
					echo "\t\t<option value=\"{$chord}\" ";
					if ($current_chord == $chord) echo "SELECTED";
					echo ">{$chord}</option>";
				}?>
				</select>
			</div>
		</form>
	</div>
</div>
<BR><BR><BR>
<BR><BR><BR>
<DIV id="results">

	<DIV id="pianoWrapper">
		<DIV id="piano"></DIV>
		Piano
	</DIV>

	<DIV id="guitarWrapper">
		<DIV id="guitar"></DIV>
		Guitar
	</DIV>

	<div id="resultsNotes">
	</div>

</DIV>



<script>
$(document).ready( function() {
	$("#keySelect").circleMenu({
		circle_radius:120,
		item_diameter:80,
	    direction:'full',
	    trigger:'click',
	    open: function(){console.log('menu opened');},
	    close: function(){console.log('menu closed');},
	    init: function(){console.log('menu initialized');},
	    select: function(evt,index){
	    	$("a#keySelectButton").text( $(index).data('note') )
				.css('background-color', $(index).css('background-color'));
	    	console.log(evt,index)
	    	buildChord();
	    }
	});
	$("#chordSelect").circleMenu({
		circle_radius:120,
		item_diameter:80,
	    direction:'full',
	    trigger:'click',
	    open: function(){console.log('menu opened');},
	    close: function(){console.log('menu closed');},
	    init: function(){console.log('menu initialized');},
	    select: function(evt,index){
	    	$("a#chordSelectButton").text( $(index).data('chord')+"/"+$(index).attr('data-note'))
				.attr('data-chord', $(index).data('chord') )
				.css('background-color', $(index).css('background-color'));
	    	console.log(evt,index)
	    	buildChord();
	    }
	});
	$('#piano').piano({
		start:0,
		keys:12
	});
	buildChord();
});


function buildChord() {
	var note = $('#keySelectButton').text();
	var scale = $('#scale').val();
	var position = $('#chordSelectButton').attr('data-chord');
	var chord = $('#chord').val();
	var string = "?k="+note+"&scale="+scale+"&pos="+position+"&chord="+chord;
	$.get('/chord'+string, function( data ) {

		//Update Chord Colors
		var index=0;
		console.log(data['scale']);
		for (var dataNote in data['scale'] ) {
			index++;
			$("#chordSelect li[data-index="+index+"]").attr('data-note', data['scale'][dataNote])
				.text( $("#chordSelect li[data-index="+index+"]").attr('data-chord')+"/"+data['scale'][dataNote] );
/*
			if (position == $("#chordSelect li[data-index="+index+"]").attr('data-chord')) {
				$("#chordSelectButton").attr('data-note', data['scale'][dataNote]);
			}
*/
		}

		//Piano
		$('.piano-key').removeClass('down').removeClass('downBase');
		for (var dataNote in data['piano'] ) {
			if ( data['piano'][dataNote] == 1 ) {
				$('.piano-'+dataNote).addClass('down').addClass('base');
			} else if ( data['piano'][dataNote] == 2) {
				$('.piano-'+dataNote).addClass('down');
			} else if ( data['piano'][dataNote] == 3) {
				$('.piano-'+dataNote).addClass('base');
			}
		}

		//Note Text
		$('#resultsNotes').html('');
		$('#resultsNotes').append('<div class="note">'+data['notes']+'</div>');


		//Guitar
		jtab.render($('#guitar'),data['chord']);

	}, 'json');
}
</script>