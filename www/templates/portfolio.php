<div class="span12">
	<h2>Portfolio</h2>
	<p>Examples of work completed by InterWebDev. Think we're a fit? <A HREF="/contact">Contact us</A>. We will work with you through every step, from speaking in easily understandable terms, to providing a detailed proposal so you know exactly what to expect during a project with us. We know it can be a little intimidating, but we really can help you and speak to you as an equal. Still not sure? Contact any of our clients for a referral.</p>
</div>

<? /* <div data-spy="scroll" data-target="#portfolioNav" data-offset="0" class="portfolioScroller"> */ ?>
<div class="portfolioScroller">
	<div id="service" class="span6"><a name="service"></a>

		<h3>Data Services</h3>
		<p>Websites that provide data as a service</p>

		<div class="media span5">
			<a class="pull-left" href="#">
				<img class="media-object" src="/assets/images/menus.jpg">
			</a>
			<div class="media-body">
				<h4 class="media-heading"><A HREF="http://devwww.menus.com" TARGET="_BLANK">Menus.com</A></h4>
				<div class="media">
					<p>Menus.com &mdash; A restaurant and menu search engine.</p>
				</div>
			</div>
		</div>

		<div class="media span5">
			<a class="pull-left" href="#">
				<img class="media-object" src="/assets/images/proxy.jpg">
			</a>
			<div class="media-body">
				<h4 class="media-heading"><A HREF="http://www.proxydumpster.com" TARGET="_BLANK">ProxyDumpster.com</A></h4>
				<div class="media">
					<p>Proxydumpster.com &mdash; An automated online proxy analyzer and statistics reporter. Built on bootstrap and custom backend framework, includes API interface.</p>
				</div>
			</div>
		</div>

	</div>


	<div id="ecommerce"><a name="ecommerce"></a>

		<h3>Ecommerce</h3>
		<p>Online product sales sites</p>

		<div class="media">
			<a class="pull-left" href="#">
				<img class="media-object" src="/assets/images/chicpeek.jpg">
			</a>
			<div class="media-body">
				<h4 class="media-heading"><A HREF="http://www.chicpeek.com" TARGET="_BLANK">ChicPeek.com</A></h4>
				<div class="media">
					<p>ChicPeek &mdash; A membership based jewelry subscription website.</p>
				</div>
			</div>
		</div>
	</div>


	<div id="community" class="span6"><a name="community"></a>

		<h3>Community</h3>
		<p>Websites driven by community</p>

		<div class="media span5">
			<a class="pull-left" href="#">
				<img class="media-object" src="/assets/images/parolejo.jpg">
			</a>
			<div class="media-body">
				<h4 class="media-heading"><A HREF="http://www.parolejo.com" TARGET="_BLANK">Parolejo.com</A></h4>
				<div class="media">
					<p>Parolejo &mdash; An esperanto speaking social media website. The site is built on Wordpress + Buddypress with custom design and custom localization and translation.</p>
				</div>
			</div>
		</div>

		<div class="media span5">
			<a class="pull-left" href="#">
				<img class="media-object" data-src="http://www.placehold.it/128x128" src="http://www.placehold.it/128x128&text=Coming+Soon">
			</a>
			<div class="media-body">
				<h4 class="media-heading"><A HREF="http://www.ebookinteractive.com" TARGET="_BLANK">EbookInteractive.com</A></h4>
				<div class="media">
					<p>Proxydumpster.com &mdash; An automated online proxy analyzer and statistics reporter. Built on bootstrap and custom backend framework, includes API interface.</p>
				</div>
			</div>
		</div>


	</div>

	<div id="nonprofit"><a name="nonprofit"></a>
		<h3>Non-profits</h3>
		<p>Websites for non-profit organizations</p>

		<div class="media">
			<a class="pull-left" href="#">
				<img class="media-object" src="/assets/images/calico.jpg">
			</a>
			<div class="media-body">
				<h4 class="media-heading"><A HREF="http://www.calicocenter.org" TARGET="_BLANK">CalicoCenter.org</A></h4>
				<div class="media">
					<p>Calico Center &mdash; Child-abuse Coordination Center.</p>
				</div>
			</div>
		</div>

	</div>

	<div id="microsites"><a name="microsites"></a>

		<h3>Microsites</h3>
		<p>Small websites</p>

		<div class="media">
			<a class="pull-left" href="#">
				<img class="media-object" src="/assets/images/chordgen.jpg">
			</a>
			<div class="media-body">
				<h4 class="media-heading"><A HREF="http://www.chordgen.com" TARGET="_BLANK">ChordGen.com</A></h4>
				<div class="media">
					<p>ChordGen &mdash; A music based API. Built with bootstrap, custom PHP framework, and an extensive amount of collected and scraped data.</p>
				</div>
			</div>
		</div>
		<div class="media">
			<a class="pull-left" href="#">
				<img class="media-object" src="/assets/images/boticus.jpg">
			</a>
			<div class="media-body">
				<h4 class="media-heading"><A HREF="http://www.boticus.com" TARGET="_BLANK">Boticus.com</A></h4>
				<div class="media">
					<p>Boticus.com &mdash; Boticus is a psuedo-random quote generator.</p>
				</div>
			</div>
		</div>

	</div>

</div>