<?
	class Request {
		static $log;
		static $error;
		static $json='';
		static $ajax=false;

		function __construct( ) {
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				self::$ajax = true;
			} else {
				self::$ajax = false;
			}
		}

		static function r($name=NULL) {
			if (! is_null($name) ) {
				if (isset($_REQUEST[$name])) {
					return $_REQUEST[$name];
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		static function md5($name=NULL) {
			return md5(self::r($name));
		}

		static function ok ($ok = false,$error = NULL,$errors = NULL) {
			self::$error = $error;
			$tn = time();
			self::$log[] = array("ok"=>$ok,"error"=>$error,"errors"=>$errors,"time"=>$tn);
			self::$json = json_encode(array(
				"ok"=>false,
				"error"=>$error
			));
		}
	}
?>