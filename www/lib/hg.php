<?php
/**
*	Hobgoblin Framework - used with ReadBeans and AltoRouter
*/

class HG {
	private static $loadedNamespaces = array();
	public static $db; //PDO DB
	public static $config;
	public static $router; //Router
	public static $stem; //Active controller
	public static $scope; //Variable scope for template

	static function init() { //Construct
		global $config,$db,$router;
		session_start();
		include_once("router.php"); //Request router
		include_once("util.php"); //Collection of utility functions
		include_once("validate.php"); //Request validation library
		include_once("rb.php"); //Readbean ORM
		include_once("request.php");
		include_once("session.php"); //Session Handler
		$router = new router();

		R::setup($config["db"]["dsn"], $config["db"]["user"],$config["db"]["password"]);

		//Injection what?
		self::$router = $router;
		self::controllers_load(); //Bootstrap controllers
	}

	static function trace() {
		echo "<!--";
		print_r(debug_backtrace());
		echo "-->";
	}

	static function force($code=NULL) { //Force a specific HTTP response
		header('HTTP/1.0 404 Not Found');
		echo "<H1>404</H1><quote>Page not foundy</quote>";
		die();
	}

	static function v($name=NULL,$val=NULL) { //Get/Set scope variables, used with templating
		if (is_null($val)) return self::$scope[$name];
		if (strstr($name,".")) {
			$name_parts = explode(".",$name);
			$name = $name_parts[0];
			if (is_array(self::$scope[$name])) {
				$sub = self::$scope[$name];
			} else {
				$sub = array();
			}
			$sub[$name_parts[1]] = $val;
			$val = $sub;
		}
		self::$scope[$name] = $val;
		return new static;
	}

	static function buildScope() {
		global $config;
		self::$scope["config"] = $config;
	}

	static function chain() {
		return new static;
	}

	static function active($target) {
		HG::v("active",array(
			"current"=>$target,
			$target=>"active"
		));
		return new static;
	}

	static function display($template,$headerfooter=false) { //Display a template file, extract scope vars into template
		self::buildScope();
		extract(self::$scope);
		if ($headerfooter === true) {
			include_once($config["templates"]."/header.php");
			include_once($config["templates"]."/".$template);
			include_once($config["templates"]."/footer.php");
		} else {
			include_once($config["templates"]."/".$template);
		}
	}

	static function controllers_load() { //Load and init controllers
		$prefix = "controllers/";
		$controllers = glob($prefix."*");
		foreach($controllers as $controller) {
			$pi = pathinfo($controller);
			$controller_base = $pi["filename"];
			if (file_exists($prefix.$controller_base."/".$controller_base.".php")) {
				include_once($prefix.$controller_base."/".$controller_base.".php");
				$controller_name = "control_".$controller_base;
				$reflection = new ReflectionMethod($controller_name,'HGInit');
				if ($reflection->isStatic()) {
					$controller_name::HGInit();
				}
			}
		}
	}

	static function redirect($location="/") { //Redirect
		header("Location: {$location}");
		die();
	}

	static function execute($match) { //Execute route target
		if (is_string($match["target"])) {
			if (strstr($match["target"],"#")) {
				$match["target"] = explode("#",$match["target"]);
			} elseif (is_callable($match["target"])) {
				call_user_func($match["target"],$match["params"]);
			}
		}
		if (is_array($match["target"])) {
			if (class_exists($match["target"][0])) {
				self::$stem = new $match["target"][0]();
				call_user_func(array(self::$stem,$match["target"][1]),$match["params"]);
			}
		}
	}

	static function __callStatic($func,$params) { //Singleton Facade Overload
		//Static calls to the HG base can be routed to any of its subsequent sections
		if (is_callable(array(self::$db,$func))) { //DB Facade
			return call_user_func_array(array(self::$db,$func),$params);
		} elseif (is_callable(array(self::$router,$func))) { //Router Facade
			return call_user_func_array(array(self::$router,$func),$params);
		}
	}
}
?>
